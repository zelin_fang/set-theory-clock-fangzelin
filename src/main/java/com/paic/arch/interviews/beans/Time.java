package com.paic.arch.interviews.beans;

import com.paic.arch.interviews.exception.CouldNotParseToTimeException;
import org.apache.commons.lang.StringUtils;

/**
 * Created by zelin on 2018/3/7.
 */
public class Time {

    public Time(){}

    /**
     * pattern HH:mm:ss
     * @param time
     */
    public Time(String time){
        if(StringUtils.isBlank(time)){
            throw new CouldNotParseToTimeException("time param  is null");
        }

        String[] split = time.split(":");
        if(split.length != 3){
            throw new CouldNotParseToTimeException("time param  is :"+time);
        }

        hour = Integer.parseInt(split[0].trim());
        minute = Integer.parseInt(split[1].trim());
        seconds = Integer.parseInt(split[2].trim());

    }

    private int hour;
    private int minute;
    private int seconds;


    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }


}
