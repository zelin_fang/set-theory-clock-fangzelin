package com.paic.arch.interviews.impl;

import com.paic.arch.interviews.TimeConverter;
import com.paic.arch.interviews.beans.Time;

/**
 * 1、将字符串转换为时分秒
 * 2、分别获取时分秒表示的字符串
 * 3、充分考虑代码复用问题，简化代码
 * Created by zelin on 2018/3/7.
 */
public class TimeConverterImpl implements TimeConverter {
    @Override
    public String convertTime(String aTime) {
        Time time = new Time(aTime);//先将字符串转换为对应的时分秒
        String ret = getSecondsClock(time.getSeconds())+getHourClock(time.getHour())+getMinuteClock(time.getMinute());
        return ret.trim();
    }

    private String getSecondsClock(int seconds){
        if(seconds >=60 || seconds < 0){
            throw new IllegalArgumentException(seconds+" is   Illegal seconds  ");
        }
        if(seconds%2 == 0){
            return "Y\r\n";
        }
        return "O\r\n";
    }

    private String getHourClock(int hour){
        if(hour >24 || hour< 0){
            throw new IllegalArgumentException(hour+" is   Illegal hour  ");
        }
        int first = hour/5;//第一批，一个R为5小时
        int second = hour%5;//第二排，一个R为1小时
        String ret = "";//返回结果
        ret += wrapResultString(first,4,"R");
        ret += wrapResultString(second,4,"R");

        return ret;
    }

    /**
     * 将计算结果转换为字符串
     * @param size
     * @param length
     * @param color
     * @return
     */
    private static String wrapResultString(int size,int length,String color){
        int tmp =size;//中间变量
        String ret = "";
        while(tmp >= 1){
            ret+=color;
            tmp--;
        }
        tmp  = length-size;
        while(tmp >= 1 ){
            ret+="O";
            tmp--;
        }
        ret = ret+"\r\n";
        return ret;
    }

    private String getMinuteClock(int  minute){
        if(minute >=60 || minute < 0 ){
            throw new IllegalArgumentException(minute+" is   Illegal minute  ");
        }
        int first = minute/5;
        int second = minute%5;
        int tmp = first;
        String ret = "";
        while(tmp >= 1){
            if(tmp == 3 || tmp == 6 || tmp ==9){
                ret = "R"+ret;
            }else {
                ret = "Y"+ ret;
            }
            tmp--;
        }
        tmp  = 11 - first;
        while(tmp >= 1 ){
            ret+="O";
            tmp--;
        }

        ret += "\r\n";
        ret += wrapResultString(second,4,"Y");
        return ret;
    }



}
