package com.paic.arch.interviews.exception;

/**
 * Created by zelin on 2018/3/7.
 */
public class CouldNotParseToTimeException extends IllegalArgumentException {

    public CouldNotParseToTimeException(String s) {
        super(s);
    }
}
